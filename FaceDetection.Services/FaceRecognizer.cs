﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using System.IO;
using FaceDetection.Entities;
using Emgu.CV.CvEnum;
using FaceDetection.Services.Utilities;
using System.Web.Hosting;
using System.Drawing.Imaging;

namespace FaceDetection.Services
{
    public class FaceRecognizer
    {

        private HaarCascade faceCascade;

        public FaceRecognizer()
        {
            faceCascade = new HaarCascade(HostingEnvironment.MapPath("~/bin/App_Data/haarcascade_frontalface_default.xml"));
        }

        /// <summary>
        /// Get the face detection information using Eigens face recognizer.
        /// </summary>
        /// <param name="imageData">The image data.</param>
        /// <returns></returns>
        public FaceDetectionData EigenFaceRecognizer(byte[] imageData)
        {
            var faceDetectionData = new FaceDetectionData();

            var originalImage = GetImageDataFromBytes(imageData);
            var grayImage = originalImage.Convert<Gray, byte>();

            // Face Detection
            var facesDetected = grayImage.DetectHaarCascade(faceCascade, 1.2, 10, HAAR_DETECTION_TYPE.DO_CANNY_PRUNING, new Size(20, 20));
            // var detectedFaces = faceCascade.Detect(grayImage, 1.2, 10, Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING, new Size(20, 20), new Size(40, 40));

            var trainingImagesEntities = GetTrainingImagesEntities();
            var trainingImages = GetImagesFromTrainingEntities(trainingImagesEntities).ToArray();
            var trainingImageNames = trainingImagesEntities.Select(x => x.Name).ToArray();

            faceDetectionData.FacesCount = facesDetected[0].Length;

            foreach (MCvAvgComp detection in facesDetected[0])
            {
                // detected face image
                var detectedFace = originalImage.Copy(detection.rect);
                var detectedFaceGray = detectedFace.Convert<Gray, byte>().Resize(100, 100, INTER.CV_INTER_CUBIC);

                // draw the face detected in the 0th (gray) channel with blue color
                originalImage.Draw(detection.rect, new Bgr(Color.Red), 2);
                var trainingImagesCount = trainingImages.Length;

                if (trainingImagesCount > 0)
                {
                    // TermCriteria for face recognition with numbers of trained images like maxIteration
                    var termCriteria = new MCvTermCriteria(trainingImagesCount, 0.001);

                    // Eigen face recognizer extention
                    var recognizer = new EigenObjectRecognizerExtention(
                       trainingImages,
                       trainingImageNames,
                       3000,
                       ref termCriteria);

                    // face recognizion duration
                    var watch = System.Diagnostics.Stopwatch.StartNew();
                    var name = recognizer.Recognize(detectedFaceGray);
                    watch.Stop();
                    faceDetectionData.Duration += watch.ElapsedMilliseconds;

                    // Draw the label for each face detected and recognized
                    var font = new MCvFont(FONT.CV_FONT_HERSHEY_TRIPLEX, 0.5d, 0.5d);
                    originalImage.Draw(name, ref font, new Point(detection.rect.X - 2, detection.rect.Y - 2), new Bgr(Color.Yellow));

                    faceDetectionData.FacesCount += 1;
                    faceDetectionData.Names.Add(name);
                    faceDetectionData.MatchingFaces.Add(detectedFace.ToBitmap().ToByteArray(ImageFormat.Bmp));

                    faceDetectionData.MatchingImage = Convert.ToBase64String(trainingImagesEntities.First(x => x.Name == name).ImageData);
                }
            }

            faceDetectionData.ImageData = originalImage.ToBitmap().ToByteArray(ImageFormat.Bmp);
            faceDetectionData.OriginalImage = Convert.ToBase64String(faceDetectionData.ImageData);

            return faceDetectionData;
        }

        private static Image<Bgr, Byte> GetImageDataFromBytes(byte[] imageData)
        {
            Bitmap bmp;
            using (var ms = new MemoryStream(imageData))
            {
                bmp = new Bitmap(ms);
            }

            Image<Bgr, Byte> originalImage = new Image<Bgr, byte>(bmp);
            return originalImage;
        }

        private static IEnumerable<TrainingImage> GetTrainingImagesEntities()
        {
            IEnumerable<TrainingImage> sampleImages;
            using (var dataContext = new FaceDataContext())
            {
                sampleImages = dataContext.TrainingImages.ToList();
            }

            return sampleImages;
        }

        private static IEnumerable<Image<Gray, byte>> GetImagesFromTrainingEntities(IEnumerable<TrainingImage> trainintEntities)
        {
            IList<Image<Gray,byte>> sampleImages = new List<Image<Gray, byte>>();

            Bitmap bmp;
            foreach (var trainingImage in trainintEntities)
            {
                using (var ms = new MemoryStream(trainingImage.ImageData))
                {
                    bmp = new Bitmap(ms);
                }

                sampleImages.Add(new Image<Gray, byte>(bmp));
            }

            return sampleImages;
        }
    }
}
