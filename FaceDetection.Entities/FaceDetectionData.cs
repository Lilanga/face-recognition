﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceDetection.Entities
{
    public class FaceDetectionData
    {
        /// <summary>
        /// Gets or sets the detected faces count.
        /// </summary>
        /// <value>
        /// The faces.
        /// </value>
        public int FacesCount { get; set; }

        /// <summary>
        /// Gets or sets the duration in miliseconds.
        /// </summary>
        /// <value>
        /// The duration.
        /// </value>
        public long Duration { get; set; }

        /// <summary>
        /// Gets or sets the names.
        /// </summary>
        /// <value>
        /// The names.
        /// </value>
        public IList<string> Names { get; set; }

        /// <summary>
        /// Gets or sets the image data.
        /// </summary>
        /// <value>
        /// The image data.
        /// </value>
        public byte[] ImageData { get; set; }

        /// <summary>
        /// Gets or sets the original image.
        /// </summary>
        /// <value>
        /// The original image.
        /// </value>
        public string OriginalImage { get; set; }

        /// <summary>
        /// Gets or sets the matching image.
        /// </summary>
        /// <value>
        /// The matching image.
        /// </value>
        /// use http://codebeautify.org/base64-to-image-converter to re create
        public string MatchingImage { get; set; }

        /// <summary>
        /// Gets or sets the matching faces.
        /// </summary>
        /// <value>
        /// The matching faces.
        /// </value>
        public IList<byte[]> MatchingFaces { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FaceDetectionData"/> class.
        /// </summary>
        public FaceDetectionData()
        {
            this.MatchingFaces = new List<byte[]>();
            this.Names = new List<string>();
        }
    }
}
