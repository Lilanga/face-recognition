﻿using FaceDetection.Entities;
using FaceDetection.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;

namespace FaceDetection.Controllers
{
    public class FaceDetectController : ApiController
    {
        // GET: api/FaceDetect
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/FaceDetect/5
        public FaceDetectionData Get(int id)
        {
            String filePath = HostingEnvironment.MapPath(string.Format("~/bin/SampleImages/face{0}.bmp", id));
            FileStream fileStream = new FileStream(filePath, FileMode.Open);

            byte[] filebytes = new byte[fileStream.Length];
            fileStream.Read(filebytes, 0, Convert.ToInt32(fileStream.Length));
            fileStream.Dispose();

            var faceRecognizer = new FaceRecognizer();
            return faceRecognizer.EigenFaceRecognizer(filebytes);
        }

        // POST: api/FaceDetect
        public FaceDetectionData Post([FromBody] string base64String)
        {
            var imageData = Convert.FromBase64String(base64String);
            var faceRecognizer = new FaceRecognizer();
            return faceRecognizer.EigenFaceRecognizer(imageData);
        }

        // PUT: api/FaceDetect/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/FaceDetect/5
        public void Delete(int id)
        {
        }
    }
}
