﻿using FaceDetection.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Hosting;
using System.Web.Http;

namespace FaceDetection.Controllers
{
    public class FaceAddController : ApiController
    {
        // GET: api/FaceAdd
        public IEnumerable<TrainingImage> Get()
        {
            IEnumerable<TrainingImage> returnValue = null;
            using (var dataContext = new FaceDataContext())
            {
                returnValue = dataContext.TrainingImages;
            }

            return returnValue;
        }

        // GET: api/FaceAdd/5
        public TrainingImage Get(int id)
        {
            //var result = new HttpResponseMessage(HttpStatusCode.OK);
            //String filePath = HostingEnvironment.MapPath(string.Format("~/bin/TrainedFaces/face{0}.bmp",id));
            //FileStream fileStream = new FileStream(filePath, FileMode.Open);
            //Image image = Image.FromStream(fileStream);
            //MemoryStream memoryStream = new MemoryStream();
            //image.Save(memoryStream, ImageFormat.Jpeg);
            //result.Content = new ByteArrayContent(memoryStream.ToArray());
            //result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");

            //return result;

            var result = new TrainingImage();
            //String filePath = HostingEnvironment.MapPath(string.Format("~/bin/TrainedFaces/face{0}.bmp", id));
            //FileStream fileStream = new FileStream(filePath, FileMode.Open);

            //byte[] filebytes = new byte[fileStream.Length];
            //fileStream.Read(filebytes, 0, Convert.ToInt32(fileStream.Length));

            //result.ImageData = filebytes;
            //result.Name = string.Format("face{0}.bmp", id);

            //fileStream.Dispose();

            using (var dataContext = new FaceDataContext())
            {
                result = dataContext.TrainingImages.FirstOrDefault(x=>x.Id==id);
            }

            return result;
        }

        // POST: api/FaceAdd
        public void Post([FromBody]TrainingImage image)
        {
            using (var dataContext = new FaceDataContext())
            {
                if (!dataContext.TrainingImages.Any(x=>x.Name == image.Name))
                {
                    dataContext.TrainingImages.Add(image);
                    dataContext.SaveChanges();
                }
            }
        }

        // PUT: api/FaceAdd/5
        public void Put(int id, [FromBody]TrainingImage image)
        {
            using (var dataContext = new FaceDataContext())
            {
                var data = dataContext.TrainingImages.FirstOrDefault(x => x.Id == id);
                if (data != null)
                {
                    data.Name = image.Name;
                    data.ImageData = image.ImageData;
                    dataContext.SaveChanges();
                }
            }
        }

        // DELETE: api/FaceAdd/5
        public void Delete(int id)
        {
            using (var dataContext = new FaceDataContext())
            {
                var data = dataContext.TrainingImages.FirstOrDefault(x => x.Id == id);
                if (data != null)
                {
                    dataContext.TrainingImages.Remove(data);
                    dataContext.SaveChanges();
                }
            }
        }
    }
}
